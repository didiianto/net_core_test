﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace net_core_test
{
    public class Mahasiswa
    {
        public int Id { get; set; }

        public string Nik { get; set; }

        public string Nama { get; set; }

        public DateTime tglLahir { get; set; }

        public string Alamat { get; set; }
    }
}

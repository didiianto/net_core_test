﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace net_core_test.LiteDb
{
    public class LiteDbOptions
    {
        public string DatabaseLocation { get; set; }
    }
}

﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace net_core_test.LiteDb
{
    public class LiteDbMahasiswaService : ILiteDbMahasiswaService
    {

        private LiteDatabase _liteDb;

        public LiteDbMahasiswaService(ILiteDbContext liteDbContext)
        {
            _liteDb = liteDbContext.Database;
        }

        public IEnumerable<Mahasiswa> FindAll()
        {
            var result = _liteDb.GetCollection<Mahasiswa>("Mahasiswa")
                .FindAll();
            return result;
        }

        public Mahasiswa FindOne(int id)
        {
            return _liteDb.GetCollection<Mahasiswa>("Mahasiswa")
                .Find(x => x.Id == id).FirstOrDefault();
        }

        public int Insert(Mahasiswa mahasiswa)
        {
            return _liteDb.GetCollection<Mahasiswa>("Mahasiswa")
                .Insert(mahasiswa);
        }

        public bool Update(Mahasiswa mahasiswa)
        {
            return _liteDb.GetCollection<Mahasiswa>("Mahasiswa")
                .Update(mahasiswa);
        }

        public int Delete(int id)
        {
            return _liteDb.GetCollection<Mahasiswa>("Mahasiswa")
                .DeleteMany(x => x.Id == id);

            //return 1;
        }
    }
}

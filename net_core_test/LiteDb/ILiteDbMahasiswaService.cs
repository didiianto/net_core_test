﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace net_core_test.LiteDb
{
    public interface ILiteDbMahasiswaService
    {
        int Delete(int id);
        IEnumerable<Mahasiswa> FindAll();
        Mahasiswa FindOne(int id);
        int Insert(Mahasiswa mahasiswa);
        bool Update(Mahasiswa mahasiswa);
    }
}

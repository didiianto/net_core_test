﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using net_core_test.LiteDb;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace net_core_test.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MahasiswaController : ControllerBase
    {
        private readonly ILogger<MahasiswaController> _logger;
        private readonly ILiteDbMahasiswaService _mahasiswaService;

        public MahasiswaController(ILogger<MahasiswaController> logger, ILiteDbMahasiswaService mahasiswaService)
        {
            _mahasiswaService = mahasiswaService;
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Mahasiswa> Get()
        {
            return _mahasiswaService.FindAll();
        }

        [HttpGet("{id}", Name = "FindOne")]
        public ActionResult<Mahasiswa> Get(int id)
        {
            var result = _mahasiswaService.FindOne(id);
            if (result != default)
                return Ok(result);
            else
                return NotFound();
        }

        [HttpPost]
        public ActionResult<Mahasiswa> Insert(Mahasiswa dto)
        {
            bool isPass = true;
            if (dto.Alamat.Length > 200)
                isPass = false;
            if(dto.Nama.Length > 20)
                isPass = false;

            bool found = _mahasiswaService.FindAll().Where(x => x.Nik.ToLower().Trim() == dto.Nik.ToLower().Trim()).Count() > 0;
            if(found)
                isPass = false;

            bool age = DateTime.Now.Year - dto.tglLahir.Year < 17;
            if(age)
                isPass = false;

            int id = 0;
            if(isPass)
                id = _mahasiswaService.Insert(dto);

            if (id != default && isPass)
                return Ok(dto);
            else
                return BadRequest();
        }

        [HttpPut]
        public ActionResult<Mahasiswa> Update(Mahasiswa dto)
        {
            var result = _mahasiswaService.Update(dto);
            if (result)
                return Ok();
            else
                return BadRequest();
        }

        [HttpDelete("{id}")]
        public ActionResult<Mahasiswa> Delete(int id)
        {
            var result = _mahasiswaService.Delete(id);
            if (result > 0)
                return Ok();
            else
                return BadRequest();
        }
    }
}
